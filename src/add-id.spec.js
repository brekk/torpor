import { addId, formatPath } from './add-id'

test('addId', () => {
  expect(addId({})).toEqual({})
  const added = addId({ dir: 'OH/man/WHAT_the/hell' })
  expect(Object.keys(added)).toEqual(['dir', 'id'])
  const version = added.id.split('-')[0]
  expect(added.id.split('::')[0].substr(version.length + 1)).toEqual(
    'oh-man-what_the-hell'
  )
  expect(added.id).toMatchSnapshot()
})

test('formatPath', () => {
  const formatted = formatPath('a/b/c/d/e')
  expect(formatted).toMatchSnapshot()
})
