import path from 'path'
import fs from 'fs'
import { map, pipe, curry } from 'ramda'
import {
  wherePropExistsAndIsWithin,
  isTemp,
  isAction,
  lastSlash,
  isNumber,
  box,
  within,
  callbackToTheFuture,
  readOrWrite,
  proxifyN,
  cbPassFail,
  fork
} from './utils'

const cbUnary = callbackToTheFuture(1)
const cbBinary = callbackToTheFuture(2)
const cbTertiary = callbackToTheFuture(3)
const cbUnaryPassFail = cbPassFail(1)
const cbBinaryPassFail = cbPassFail(2)
const cbTertiaryPassFail = cbPassFail(3)

const here = x => path.resolve(process.cwd(), x)

const testUnary = (t, unary) =>
  test(t, done => {
    const rando = Math.round(Math.random() * 1e6)
    const access = (z, cb) =>
      fs.access(z, fs.constants.R_OK, e =>
        e ? cb(e) : cb(null, rando)
      )

    const exists = unary(access)
    const ff = exists(here('./package.json'))
    fork(
      done,
      z => {
        expect(z).toEqual(rando)
        done()
      },
      ff
    )
  })
testUnary('cbUnary', cbUnary)
testUnary('proxifyN(1)', proxifyN(1))
const failUnary = (t, unary) =>
  test(t, done => {
    const rando = Math.round(Math.random() * 1e6)
    const access = curry((z, cb) =>
      fs.access(z, fs.constants.R_OK, e =>
        e ? cb(e) : cb(null, rando)
      )
    )
    const exists = unary(access)
    fork(
      z => {
        expect(z.message.split('access')[0]).toEqual(
          'ENOENT: no such file or directory, '
        )
        done()
      },
      done,
      exists(here('./fake.json'))
    )
  })
failUnary('cbUnary', cbUnary)
failUnary('proxifyN(1)', proxifyN(1))
const testBinary = (t, fn) =>
  test(t, done => {
    const bin = fn(fs.readFile)
    pipe(
      map(JSON.parse),
      fork(done, z => {
        expect(z.name).toEqual('torpor')
        done()
      })
    )(bin(here('./package.json'), 'utf8'))
  })
testBinary('cbBinary', cbBinary)
testBinary('proxifyN(2)', proxifyN(2))
const failBinary = (t, fn) =>
  test(t, done => {
    const bin = fn(fs.readFile)
    pipe(
      map(JSON.parse),
      fork(z => {
        expect(z.message.split('open')[0]).toEqual(
          'ENOENT: no such file or directory, '
        )
        done()
      }, done)
    )(bin(here('./fake.json'), 'utf8'))
  })
failBinary('cbBinary', cbBinary)
failBinary('proxifyN(2)', proxifyN(2))
const testTertiary = (t, fn) =>
  test(t, done => {
    const f = fn(fs.open)

    pipe(
      map(JSON.parse),
      fork(done, z => {
        expect(typeof z).toEqual('number')
        done()
      })
    )(f(here('./package.json'), 'r', 0o666))
  })
testTertiary('cbTertiary', cbTertiary)
testTertiary('proxifyN(3)', proxifyN(3))
const failTertiary = (t, fn) =>
  test(t, done => {
    const f = fn(fs.open)

    pipe(
      map(JSON.parse),
      fork(z => {
        expect(z.message.split('open')[0]).toEqual(
          'ENOENT: no such file or directory, '
        )
        done()
      }, done)
    )(f(here('./fake.json'), 'r', 0o666))
  })
failTertiary('cbTertiary', cbTertiary)
failTertiary('proxifyN(3)', proxifyN(3))
test('cbUnaryPassFail', done => {
  const rando = Math.round(Math.random() * 1e6)
  const fn = (a, cb) => cb(null, a + rando)
  const x = 100
  fork(
    done,
    z => {
      expect(z).toBeTruthy()
      done()
    },
    cbUnaryPassFail(fn)(x)
  )
})
test('cbUnaryPassFail - fail', done => {
  const rando = Math.round(Math.random() * 1e6)
  const fn = (a, cb) => {
    cb(new Error(rando))
  }
  const x = 100
  fork(
    z => {
      expect(z).toBeFalsy()
      done()
    },
    done,
    cbUnaryPassFail(fn)(x)
  )
})
test('cbBinaryPassFail', done => {
  const rando = Math.round(Math.random() * 1e6)
  const fn = (a, b, cb) => cb(null, a + b + rando)
  const x = 100
  fork(
    done,
    z => {
      expect(z).toBeTruthy()
      done()
    },

    cbBinaryPassFail(fn)(x, x)
  )
})
test('cbBinaryPassFail - fail', done => {
  const rando = Math.round(Math.random() * 1e6)
  const fn = (a, b, cb) => {
    cb(new Error(rando))
  }
  const x = 100
  fork(
    z => {
      expect(z).toBeFalsy()
      done()
    },
    done,
    cbBinaryPassFail(fn)(x, x)
  )
})
test('cbTertiaryPassFail', done => {
  const rando = Math.round(Math.random() * 1e6)
  const fn = (a, b, c, cb) => cb(null, a + b + c + rando)
  const x = 100
  fork(
    done,
    z => {
      expect(z).toBeTruthy()
      done()
    },
    cbTertiaryPassFail(fn)(x, x, x)
  )
})
test('cbTertiaryPassFail - fail', done => {
  const rando = Math.round(Math.random() * 1e6)
  const fn = (a, b, c, cb) => {
    cb(new Error(rando))
  }
  const x = 100
  fork(
    z => {
      expect(z).toBeFalsy()
      done()
    },
    done,

    cbTertiaryPassFail(fn)(x, x, x)
  )
})

test('readOrWrite', done => {
  const rando = (z = 1e3) => Math.round(Math.random() * z + 1)
  const myFunc = (fd, buff, off, len, pos, cb) => {
    cb(null, { fd, buff, off, len, pos })
  }
  const orFn = readOrWrite(myFunc)
  const data = [rando(), rando(), rando(), rando(), rando()]
  fork(
    done,
    x => {
      expect(x.fd).toEqual(data[0])
      expect(x.buff).toEqual(data[1])
      expect(x.off).toEqual(data[2])
      expect(x.len).toEqual(data[3])
      expect(x.pos).toEqual(data[4])
      done()
    },
    orFn(...data)
  )
})
test('readOrWrite - fail', done => {
  const rando = (z = 1e3) => Math.round(Math.random() * z) + 1
  const given = rando()
  const myFunc = (fd, buff, off, len, pos, cb) => {
    cb(new Error(given))
  }
  const orFn = readOrWrite(myFunc)
  const data = [rando(), rando(), rando(), rando(), rando()]
  fork(
    z => {
      expect(parseInt(z.message, 10)).toEqual(given)
      done()
    },
    done,
    orFn(...data)
  )
})

test('within', () => {
  expect(within('cool', 'oh man so cool and exciting')).toBeTruthy()
  expect(within('fool', 'oh man so cool and exciting')).toBeFalsy()
})

test('box', () => {
  expect(box('a')).toEqual(['a'])
  expect(box(['a', 'b'])).toEqual(['a', 'b'])
})
// test("pInt", () => {
//   expect(pInt("23049303493")).toEqual(23049303493)
// })

test('isNumber', () => {
  expect(isNumber('023948203948203984')).toBeTruthy()
  expect(isNumber('scrapbkldfjldkj')).toBeFalsy()
})

test('lastSlash', () => {
  expect(lastSlash('a/b/c/d/e/cool')).toEqual('cool')
})

test('isTemp', () => {
  expect(isTemp('slkdfjslkdjfljk~')).toBeTruthy()
  expect(isTemp('slkdfjslkdjfljk')).toBeFalsy()
})
test('isAction', () => {
  expect(isAction(1, 1)).toBeTruthy()
  expect(isAction(23092209, 1)).toBeFalsy()
})
test('wherePropExistsAndIsWithin', () => {
  const where = false
  const y = 'y?'
  const x = "why'd you do that? y? jerk"
  const actual = wherePropExistsAndIsWithin(where, y, x)
  expect(actual).toBeTruthy()
  const actual2 = wherePropExistsAndIsWithin('x', y, { x })
  expect(actual2).toBeTruthy()
  const falsy = wherePropExistsAndIsWithin('x', 'testo!', { x })
  expect(falsy).toBeFalsy()
})
