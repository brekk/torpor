import { Par } from 'fluture'
import { curry, pipe } from 'ramda'
import {
  close as _close,
  fdatasync as _fdatasync,
  fsync as _fsync,
  rmdir as _rmdir,
  unlink as _unlink,
  access as _access,
  chmod as _chmod,
  fchmod as _fchmod,
  fstat as _fstat,
  ftruncate as _ftruncate,
  lchmod as _lchmod,
  link as _link,
  mkdir as _mkdir,
  rename as _rename,
  truncate as _truncate,
  appendFile as _appendFile,
  chown as _chown,
  copyFile as _copyFile,
  fchown as _fchown,
  futimes as _futimes,
  lchown as _lchown,
  symlink as _symlink,
  utimes as _utimes,
  writeFile as _writeFile
} from './fs'

const para1 = fn => pipe(fn, Par.of)
const para2 = curry(
  /* istanbul ignore next */
  (fn, x, y) => pipe(fn(x), Par.of)(y)
)

const para3 = curry(
  /* istanbul ignore next */
  (fn, x, y, z) => pipe(fn(x, y), Par.of)(z)
)

export const close = para1(_close)
export const fdatasync = para1(_fdatasync)
export const fsync = para1(_fsync)
export const rmdir = para1(_rmdir)
export const unlink = para1(_unlink)

export const access = para2(_access)
export const chmod = para2(_chmod)
export const fchmod = para2(_fchmod)
export const fstat = para2(_fstat)
export const ftruncate = para2(_ftruncate)
export const lchmod = para2(_lchmod)
export const link = para2(_link)
export const mkdir = para2(_mkdir)
export const rename = para2(_rename)
export const truncate = para2(_truncate)

export const appendFile = para3(_appendFile)
export const chown = para3(_chown)
export const copyFile = para3(_copyFile)
export const fchown = para3(_fchown)
export const futimes = para3(_futimes)
export const lchown = para3(_lchown)
export const symlink = para3(_symlink)
export const utimes = para3(_utimes)
export const writeFile = para3(_writeFile)
