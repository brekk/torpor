import path from 'path'
import {
  pipe,
  propOr,
  keys,
  objOf,
  mergeRight as merge,
  toLower,
  replace
} from 'ramda'
import { camelCase } from 'camel-case'
import pkg from '../package.json'
import { unusualString } from './utils'

const PATH_SEPARATOR = new RegExp(path.sep, 'g')
export const formatPath = pipe(
  toLower,
  replace(PATH_SEPARATOR, '-'),
  x => `${camelCase(pkg.version)}-${x}`
)

const addChance = x => `${x}::${unusualString()}`

export const addKey = raw =>
  pipe(
    propOr('', 'dir'),
    formatPath,
    addChance,
    objOf('id'),
    merge(raw)
  )(raw)

export const addId = z => (keys(z).length === 0 ? z : addKey(z))

export default addId
