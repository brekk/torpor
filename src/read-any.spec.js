import path from 'path'
import { after } from 'fluture'
import { map, pipe } from 'ramda'
import { readAny, readAnyWithFormatOr } from './read-any'
import { fork } from './utils'

const here = z => path.resolve(__dirname, '..', z)

test('readAny', done => {
  const sources = [
    'package.json',
    'node_modules/fake-module/package.json'
  ].map(here)
  pipe(
    readAny,
    map(JSON.parse),
    fork(done)(z => {
      expect(z.name).toEqual('torpor')
      done()
    })
  )(sources)
})
test('readAnyWithFormatOr - default is fluture', done => {
  const rando = Math.round(Math.random() * 1e3)
  const fourchette = readAnyWithFormatOr(after(20)(rando), 'utf8', [
    'blahblahblah.js',
    'blahblahblah2.js'
  ])
  fork(done)(z => {
    console.log('ZZZZZ2', z)
    expect(z).toEqual(rando)
    done()
  })(fourchette)
})
test.skip('readAny - invalid option', done => {
  const sources = [
    'node_modules/fake-module/package.json',
    'node_modules/xtrace/package.json'
  ].map(here)
  const rawF = readAny(sources)
  pipe(
    map(JSON.parse),
    fork(done)(z => {
      console.log('ZZZZZ3', z)
      expect(z.name).toEqual('xtrace')
      done()
    })
  )(rawF)
})
test.skip('readAny - only invalid options', done => {
  const sources = [
    'node_modules/fake-module/package.json',
    'node_modules/fake-module-zwei/package.json'
  ].map(here)

  const fourchette = readAny(sources)
  pipe(
    map(JSON.parse),
    fork(e => {
      console.log('ZZZZZ4', e)
      expect(e.message.split('open')[0]).toEqual(
        'ENOENT: no such file or directory, '
      )
      done()
    })(done)
  )(fourchette)
})
