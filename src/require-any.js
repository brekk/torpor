import { __ as $ } from 'ramda'
import { constants, access } from './fs'
import { findF } from './utils'

export const requireAny = findF(access($, constants.R_OK))
