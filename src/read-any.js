import { curry } from 'ramda'
import { readFile } from './fs'
import { findF } from './utils'

/**
 * @typedef {Object} Future
 * @property {function} bimap
 * @property {function} chain
 * @property {function} fold
 * @property {function} fork
 * @property {function} map
 * @property {function} mapRej
 * @property {function} pipe
 * @property {function} swap
 * It's a future
 */

/**
 * @function readAnyWithFormatOr
 * @param {*} def - default
 * @param {string} format - probably utf8?
 * @param {Array<string>} sources - a list of possible file sources
 * @returns {Future} the first matching file which resolves
 */
export const readAnyWithFormatOr = curry((def, format, x) =>
  findF(y => readFile(y, format), def, x)
)

/**
 * @function readAnyWithFormat
 * @param {string} format - probably utf8?
 * @param {Array<string>} sources - a list of possible file sources
 * @returns {Future} the first matching file which resolves
 */
export const readAnyWithFormat = readAnyWithFormatOr(null)

/**
 * @function readAny
 * @param {Array<string>} sources - a list of possible file sources
 * @returns {Future} the first matching file which resolves
 */
export const readAny = readAnyWithFormat('utf8')
