import path from 'path'
import {
  mapRej,
  isFuture,
  race,
  Future,
  fork as rawFork
} from 'fluture'
import {
  curry,
  curryN,
  identity as I,
  join,
  map,
  memoizeWith,
  pipe,
  prop,
  propOr,
  reduce,
  times
} from 'ramda'
// import nsfw from 'nsfw'
import Unusual from 'unusual'
import pkg from '../package.json'
export const unusual = new Unusual(pkg.name + '@' + pkg.version)
const alphabet = 'abcdefghijklmnopqrstuvwxyz'
const strings = unusual.shuffle(
  `${alphabet}${alphabet.toString()}0123456789`.split('')
)
export const unusualString = () =>
  join(
    '',
    times(
      () => unusual.pick(strings),
      unusual.integer({ min: 5, max: 20 })
    )
  )

export const fork = curry((bad, good, x) => rawFork(bad)(good)(x))

/**
 * takes an arity and nodeback function
 * and returns a future-returning function
 * NB: arity in this case doesn't take the callback into the count
 * so arity = 2 means a function with the shape (a, b, callback) => {}
 *
 * @function callbackToTheFuture
 * @param {number} arity - number of params
 * @param {Function} fn - a nodeback-style function
 * @returns {Function} Future-returning function
 * @example
 * import fs from "fs"
 * import { callbackToTheFuture, fork } from "torpor/utils"
 * import { trace } from "xtrace"
 * const readFile = callbackToTheFuture(2, fs.readFile)
 * fork(trace("bad"), trace("good"), readFile("myfile.txt", "utf8"))
 */
export const callbackToTheFutureWithCancel = curry(
  (cancel, arity, fn) =>
    curryN(arity, function warp(...args) {
      return new Future((bad, good) => {
        const newArgs = [...args, (e, f) => (e ? bad(e) : good(f))]
        fn.apply(this, newArgs)
        return cancel
      })
    })
)
export const callbackToTheFuture = callbackToTheFutureWithCancel(
  () => {}
)

/**
 * takes an arity and nodeback function
 * and returns a curried future-returning function
 * NB: arity in this case doesn't take the callback into the count
 * so arity = 2 means a function with the shape (a, b, callback) => {}
 *
 * @function proxifyN
 * @param {number} arity - arity
 * @param {Function} fn - a nodeback style function
 * @returns {Function} curried Future-returning function
 * @see callbackToTheFuture
 * @example
 * import fs from "fs"
 * import { proxifyN, fork } from "torpor/utils"
 * import { trace } from "xtrace"
 * import { pipe, map, __ as $ } from "ramda"
 * const readFile = proxifyN(2, fs.readFile)
 * const utf8 = readFile($, "utf8")
 * pipe(map(JSON.parse), fork(trace("bad"))(trace("good")))(utf8("package.json"))
 */
export const proxifyN = curry((arity, fn) =>
  pipe(curryN(arity), callbackToTheFuture(arity))(fn)
)

export const cbPassFailWithCancel = curry((cancel, arity, fn) =>
  curryN(arity, function passfail(...args) {
    return new Future((bad, good) => {
      const newArgs = [...args, e => (e ? bad(false) : good(true))]
      fn.apply(this, newArgs)
      return cancel
    })
  })
)

export const cbPassFail = cbPassFailWithCancel(
  /* istanbul ignore next */
  () => {}
)

export const readOrWriteWithCancel = curry(
  (cancel, fn, fd, buffer, offset, len, position) =>
    new Future((bad, good) => {
      fn(fd, buffer, offset, len, position, (e, bytes, buff) =>
        e ? bad(e) : good(bytes, buff)
      )
      return cancel
    })
)
export const readOrWrite = readOrWriteWithCancel(
  /* istanbul ignore next */
  () => {}
)
export const memo = memoizeWith(I)
export const within = curry((y, x) => ~x.indexOf(y))
export const box = z => (Array.isArray(z) ? z : [z])
const pInt = memo(z => parseInt(z, 10))
export const isNumber = pipe(pInt, z => !isNaN(z))

export const lastSlash = memo(z =>
  z.substr(z.lastIndexOf(path.sep) + 1)
)
export const isTemp = memo(z => z[z.length - 1] === `~`)
export const UNKNOWN = `UNKNOWN`
export const propU = propOr(UNKNOWN)
export const isAction = curry((a, x) => x === a)
/*
{
  CREATED: 0,
  DELETED: 1,
  MODIFIED: 2,
  RENAMED: 3
}
*/
// export const isCreated = isAction(nsfw.actions.CREATED)
// export const isModified = isAction(nsfw.actions.MODIFIED)
// export const isDeleted = isAction(nsfw.actions.DELETED)
// export const isRenamed = isAction(nsfw.actions.RENAMED)
// export const getFile = curry((action, previous, current) =>
//   isDeleted(action)
//     ? { previous: current }
//     : previous && current
//     ? {
//         previous,
//         current
//       }
//     : current
//     ? { current }
//     : { previous }
// )

export const existsAndIsWithin = curry((y, x) => x && within(y, x))

export const wherePropExistsAndIsWithin = curry((where, y, x) =>
  pipe(where ? prop(where) : I, existsAndIsWithin(y))(x)
)

export const findF = curry((fn, def, x) =>
  pipe(
    map(fn),
    map(mapRej(() => false)),
    reduce((a, b) => (isFuture(a) ? race(a)(b) : b), def)
  )(x)
)
