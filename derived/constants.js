const BASE_URI = 'https://nodejs.org/api/fs.html'

const H2 = '<h2'

const C = {
  _: ' ',
  n: '\n',
  star: '\n * ',
  estar: '\n *'
}
C.starstar = C.estar + C.star
const FLUTURE_LINK = `[fluture](https://github.com/fluture-js/Fluture)`

module.exports = { C, H2, FLUTURE_LINK, BASE_URI }
