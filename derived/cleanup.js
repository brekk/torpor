const { pipe, prop, replace, split } = require('ramda')
const { C } = require('./constants')

const cleanup = pipe(
  split(C.n),
  prop(0),
  replace(/fs\./, ''),
  // replace(/Sync/, ""),
  replace(/\\\[/, ''),
  replace(/\\\]/, ''),
  replace(/\(#fs_(.*?)\)/, ''),
  replace('## ', '@function '),
  replace(/\[\\#\]/, ''),
  replace(/\(htt/, 'htt'),
  split(`[\\[src\\]]`)
)
module.exports = cleanup
