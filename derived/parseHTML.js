const R = require('ramda')
// const { traceWithScopeWhen, trace } = require("xtrace")
const { H2, C } = require('./constants')
const getDescription = require('./get-description')
const getName = require('./get-name')
const getOurDescription = require('./get-our-description')
const getParams = require('./get-params')
const getReturn = require('./get-return')
const getSourceLink = require('./get-source-link')
const { convert } = require('./html2markdown')

const {
  ap,
  curry,
  filter,
  identity,
  join,
  last,
  map,
  pipe,
  reduce,
  reject,
  split,
  trim
} = R

// const whereIsReadFile = L => traceWithScopeWhen(() => L === "fs.readFile")

const parsify = curry((x, L) =>
  pipe(
    // replace(/\n\n/g, C.n),
    split(/\n/g),
    map(trim),
    map(split(H2)),
    // group stuff between H2s
    reduce((a, b) => {
      if (a.length === 0) return [b[0]]
      const isStart = b.length > 1
      const bb = isStart ? H2 + b[1] : b[0]
      const rest = a.slice(0, -1)
      const lastA = last(a)
      const isLast = Array.isArray(lastA)
      if (isStart && isLast) return [...a, [bb]]
      if (isStart) return [a, [bb]]
      if (isLast) return [...rest, lastA.concat(bb)]
      return a.concat(bb)
    }, []),
    // filter based on user input
    L ? filter(([z]) => z.indexOf('<h2>' + L + '(') === 0) : identity,
    map(join(C.n)),
    // prop(0),
    // convert to markdown
    map(convert),
    reject(z => z.substr(0, z.indexOf('(')).indexOf('Sync') > -1),
    // flatten bullet points
    map(z => z.replace(/\n\* \n/g, '\n* ')),
    // grab a few different pieces of information
    ap([
      getOurDescription,
      getDescription,
      getName,
      getParams,
      getReturn,
      getSourceLink
    ]),
    // join them together
    join(C.star),
    z => `/**\n * ${z}\n */`
  )(x)
)

module.exports = parsify
