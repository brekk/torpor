const { curry, head, match, pipe, prop } = require('ramda')
const { BASE_URI, FLUTURE_LINK } = require('./constants')

const getGenericDescription = curry((name, x) =>
  pipe(
    head,
    match(/\(#fs_(.*?)\)/),
    prop(1),
    z =>
      `Wraps [${name}](${BASE_URI}#fs_${z})
 * with ${FLUTURE_LINK} so that instead of taking a
 * callback it returns a Future.`
  )(x)
)

module.exports = getGenericDescription
