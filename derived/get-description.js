const {
  join,
  map,
  pipe,
  reduce,
  reject,
  replace,
  split
} = require('ramda')
const { C, BASE_URI } = require('./constants')

const getDescription = pipe(
  split(C.n + C.n),
  reject(
    z =>
      z.indexOf('*') === 0 || z === 'History' || z.indexOf('##') === 0
  ),
  reduce(
    (a, b) =>
      b.length < 100
        ? a.concat(b)
        : a.concat(
            b
              .split('. ')
              .map(z =>
                z.lastIndexOf('.') !== z.length - 1 ? z + '.' : z
              )
          ),
    []
  ),
  map(replace(/\n/g, C.star + '> ')),
  join(C.starstar + '> '),
  z =>
    C.star +
    `via [node API docs](${BASE_URI}):` +
    C.starstar +
    '> ' +
    z +
    C.estar
)

module.exports = getDescription
