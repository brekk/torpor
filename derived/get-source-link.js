const { pipe, prop, replace, split } = require('ramda')
const { traceWithScopeWhen, trace } = require('xtrace')
const { C } = require('./constants')

const getSourceLink = pipe(
  split(C.n),
  prop(0),
  replace(/\(#fs_(.*?)\)/, ''),
  replace(/\[\\#\]/, ''),
  replace(/\(htt/, 'htt'),
  split(`[\\[src\\]]`),
  traceWithScopeWhen((_, x) => !x, prop(0), '>>>>>>>>>>>'),
  ([, see]) => see,
  z =>
    z
      ? `@see {@link ${z.substr(
          0,
          z.length - 1
        )}}\n * for the source function this function wraps.`
      : ''
)

module.exports = getSourceLink
