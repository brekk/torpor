const {
  curry,
  filter,
  join,
  last,
  map,
  pipe,
  reduce,
  reject,
  replace,
  split
} = require('ramda')

const { C } = require('./constants')
const matches = curry((m, z) => z.indexOf(m) > -1)

const getParams = pipe(
  split(C.n),
  filter(z => z.indexOf('*') === 0 || z.indexOf('  *') === 0),
  map(
    pipe(
      replace(/`/g, ''),
      // strip links
      replace(/\(.*?\)/g, ''),
      // replace "] | [" with "|"
      replace(/\]\s\|\s\[/g, '|'),
      // classily replace "[]" and defer for later
      replace(/\\\[\\\]/g, '⫏'),
      // turn [<a/>] => [a]
      replace(/[<.*?\\>]/gu, '\x01'),
      // turn [ => {
      replace(/\[/g, '{'),
      // turn ] => } -
      replace(/\]/g, '} -'),
      // replace this deeply irritating flaw
      replace('\u0001', ''),
      // remap 'Returns: ' => @returns, copy the type signature into the description
      z => {
        const y = replace(/Returns: /g, '@returns ', z)
        if (z === y) return z
        const type = y.slice(0, -3)
        return (
          type +
          '} returns ' +
          type.substr(type.indexOf('{') + 1).replace(/\|/g, ' or ')
        )
      },
      // add @param
      z =>
        matches('@returns', z) ? z.replace(' @', '@') : `@param` + z,
      // deal with that problem we deferred earlier
      replace(/⫏/g, '[]')
    )
  ),
  reduce((a, b) => {
    const lastLine = a.slice(-1)[0]
    let last = lastLine
      ? lastLine.slice(
          lastLine.indexOf(' '),
          lastLine.indexOf('{') - 1
        )
      : ''
    const isNestedParam = b.indexOf('@param   ') > -1
    if (last.indexOf('.') > -1)
      last = last.substr(0, last.indexOf('.'))
    if (lastLine && last && isNestedParam) {
      const c = b.replace('   ', last + '.')
      return a.concat(c)
    }
    return a.concat(b)
  }, []),
  map(
    replace(
      /@param (?<name>(.*?)) \{(?<type>(.*?))\} -/,
      (...args) => {
        const lastA = last(args)
        const { type, name } = lastA
        const cleanType = type === 'integer' ? 'number' : type
        return `@param {${cleanType}} ${name} -`
      }
    )
  ),
  reject(matches('callback')),
  join(C.star)
)

module.exports = getParams
