const path = require('path')
const { fork } = require('fluture')
const { trace } = require('xtrace')
const { map, includes, identity, join, pipe } = require('ramda')
const torpor = require('../torpor')
const { C } = require('./constants')
const parseHTML = require('./parseHTML')

const here = z => path.resolve(__dirname, z)

const argv = process.argv.slice(2)
const [lookup] = argv

const readFS = torpor.readFile(here('fs.html'), 'utf8')

const hasSpaces = includes(' ')

pipe(
  map(file =>
    hasSpaces(lookup)
      ? map(parseHTML(file))(lookup.split(' '))
      : parseHTML(file, lookup)
  ),
  map(hasSpaces ? join(C.n) : identity),
  fork(trace('bad'))(console.log)
)(readFS)
