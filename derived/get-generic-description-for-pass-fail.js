const { equals, find, length, pipe } = require('ramda')

const getGenericDescriptionForPassFail = pipe(
  find(
    z =>
      z.indexOf('No arguments other than a possible exception') > -1
  ),
  length,
  equals(0),
  isGeneric =>
    !isGeneric
      ? `Unlike the raw function it wraps, this function now returns a
 * Future which wraps a value indicating whether the operation succeeded.`
      : ``
)

module.exports = getGenericDescriptionForPassFail
