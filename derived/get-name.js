const { join, pipe, replace } = require('ramda')
const cleanup = require('./cleanup')
const { C } = require('./constants')

const getName = pipe(
  cleanup,
  ([fn]) => [replace(/\((.+),?\)/g, '', fn)],
  join(C.n)
)

module.exports = getName
