const { ap, join, pipe, split } = require('ramda')
const { C } = require('./constants')
const getGenericDescription = require('./get-generic-description')
const getGenericDescriptionForPassFail = require('./get-generic-description-for-pass-fail')

const getOurDescription = raw => {
  const name = raw.substr(
    raw.indexOf('## ') + 3,
    raw.indexOf('(') - 3
  )
  return pipe(
    split(C.n),
    z => [z],
    ap([
      getGenericDescription(name),
      getGenericDescriptionForPassFail
    ]),
    join(C._)
  )(raw)
}

module.exports = getOurDescription
