const Europa = require('node-europa')
const { JSDOM } = require('jsdom')
const { curry } = require('ramda')

const { BASE_URI } = require('./constants')

const convertWithURI = curry((uri, x) => {
  const w =
    typeof window === 'undefined' ? new JSDOM(x).window : window
  const { document } = w // eslint-disable-line
  // return { w, d }
  const europa = new Europa({
    inline: true,
    baseUri: uri
  })
  return europa.convert(x)
})
const convert = convertWithURI(BASE_URI)
module.exports = { convertWithURI, convert }
