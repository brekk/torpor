const utils = require('nps-utils')
const { concurrent, series } = utils
const { nps: npsAll } = concurrent

// const seriesNPS = (...x) => `nps ` + x.join(` && nps `)
const sd = (script, description) =>
  description ? { script, description } : script

const SKIP_DEPCHECK_FOR = [
  `@babel/cli`,
  `@babel/core`,
  `@babel/plugin-syntax-dynamic-import`,
  `@babel/plugin-transform-destructuring`,
  `@babel/preset-env`,
  `babel-core`,
  `babel-jest`,
  `chokidar`,
  `clinton`,
  `depcheck`,
  `documentation`,
  `husky`,
  `jest`,
  `lint-staged`,
  `nps`,
  `prettier-eslint`,
  `xtrace`,
  `@babel/helper-builder-react-jsx`,
  `@rollup/plugin-alias`,
  `@rollup/plugin-babel`,
  `eslint-plugin-delorean`,
  `eslint-plugin-jsdoc`,
  `rollup-plugin-buble`,
  `rollup-plugin-cleanup`,
  `rollup-plugin-commonjs`,
  `rollup-plugin-json`,
  `rollup-plugin-node-resolve`,
  `rollup-plugin-progress`
]

const main = {
  scripts: {
    edit: 'vi package-scripts.js',
    dependencies: sd(
      `depcheck . --specials=bin,eslint,babel,rollup --ignores=${SKIP_DEPCHECK_FOR}`,
      `check dependencies`
    ),
    readme: sd(
      `documentation readme src/*.js --section API --access-private`,
      `regenerate the readme`
    ),
    lint: {
      description: `lint both the js and the jsdoc`,
      script: npsAll(`lint.src`, `lint.jsdoc`, `lint.project`),
      src: sd(`eslint src/*.js --env jest --fix`, `lint js files`),
      jsdoc: sd(`documentation lint src/*.js`, `lint jsdoc in files`),
      project: sd(
        `clinton --no-inherit`,
        `lint project using clinton`
      )
    },
    test: {
      description: `run all tests with coverage`,
      script: `NODE_ENV=test jest --verbose --coverage`,
      watch: sd(
        `jest --watchAll --coverage --verbose`,
        `watch tests with jests`
      ),
      ci: sd(
        `jest --verbose --modulePathIgnorePatterns src/watch.spec.*`,
        `ci and file watching don't play nice together`
      )
    },
    docs: {
      description: `auto regen the docs`,
      script: `documentation build src/**.js -f html -o docs --access private`,
      serve: sd(
        `documentation serve src/**.js`,
        `serve the documentation`
      )
    },
    bundle: sd(`rollup -c rollup.config.js`, `generate bundles`),
    build: sd(
      series(
        `babel src -d lib --ignore src/*.spec.js,src/*.fixture.js`
      ),
      `convert files individually`
    ),
    ci: sd(
      series(
        `nps generate`,
        npsAll(`lint`, `test.ci`, `dependencies`)
      ),
      `run stuff in a CI pipe`
    ),
    care: sd(
      series(
        `nps build`,
        `nps bundle`,
        npsAll(`lint`, `test`, `readme`, `readme`, `dependencies`)
      ),
      `run all the things`
    ),
    generate: series(`nps build`, `nps bundle`)
  },
  download: sd(
    'curl -sSL https://nodejs.org/api/fs.html > derived/fs.html',
    'download fs.html for documentation porpoises'
  )
}
module.exports = main
