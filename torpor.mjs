import fs from 'fs';
import { curry, curryN, pipe, memoizeWith, identity, propOr, prop, map, reduce, __ } from 'ramda';
import path from 'path';
import { fork, Future, mapRej, isFuture, race, Par } from 'fluture';
import Unusual from 'unusual';

var name = "torpor";
var version = "0.1.0";
var description = "lazy and monadic file systems";
var main = "torpor.js";
var module = "torpor.es.js";
var repository = "gitlab.com/brekk/torpor";
var author = "brekk";
var license = "ISC";
var scripts = {
	test: "nps test"
};
var devDependencies = {
	"@babel/cli": "^7.5.5",
	"@babel/core": "^7.14.6",
	"@babel/eslint-parser": "^7.14.7",
	"@babel/helper-builder-react-jsx": "^7.14.5",
	"@babel/plugin-syntax-dynamic-import": "^7.2.0",
	"@babel/plugin-transform-destructuring": "^7.5.0",
	"@babel/preset-env": "^7.5.5",
	"@rollup/plugin-alias": "^3.1.4",
	"@rollup/plugin-babel": "^5.3.0",
	"@rollup/plugin-commonjs": "^19.0.1",
	"@rollup/plugin-json": "^4.1.0",
	"@rollup/plugin-node-resolve": "^13.0.2",
	"babel-core": "^6.26.3",
	"babel-jest": "^24.8.0",
	clinton: "^0.14.0",
	depcheck: "^0.8.3",
	documentation: "^12.1.2",
	eslint: "^7.31.0",
	"eslint-config-prettier": "^6.0.0",
	"eslint-config-sorcerers": "^0.0.7",
	"eslint-config-standard": "^13.0.1",
	"eslint-plugin-babel": "^5.3.0",
	"eslint-plugin-delorean": "^0.0.1",
	"eslint-plugin-fp": "^2.3.0",
	"eslint-plugin-import": "^2.18.2",
	"eslint-plugin-jsdoc": "^15.8.0",
	"eslint-plugin-node": "^9.1.0",
	"eslint-plugin-prettier": "^3.1.0",
	"eslint-plugin-promise": "^4.2.1",
	"eslint-plugin-ramda": "^2.5.1",
	"eslint-plugin-standard": "^4.0.0",
	husky: "^3.0.4",
	jest: "^24.8.0",
	jsdom: "^15.1.1",
	"lint-staged": "^9.2.3",
	"node-europa": "^4.0.0",
	nps: "^5.9.5",
	"nps-utils": "^1.7.0",
	prettier: "^2.3.2",
	"prettier-eslint": "^12.0.0",
	rollup: "^2.53.2",
	"rollup-plugin-buble": "^0.19.8",
	"rollup-plugin-cleanup": "^3.1.1",
	"rollup-plugin-commonjs": "^10.0.2",
	"rollup-plugin-json": "^4.0.0",
	"rollup-plugin-node-resolve": "^5.2.0",
	"rollup-plugin-progress": "^1.1.1",
	sinew: "^1.0.1",
	xtrace: "^0.3.0"
};
var dependencies = {
	"camel-case": "^4.1.1",
	fluture: "^12.1.1",
	ramda: "^0.26.1",
	unusual: "^0.0.2"
};
var husky = {
	hooks: {
		"pre-commit": [
			"nps care"
		]
	}
};
var files = [
	"torpor.js",
	"torpor.mjs"
];
var pkg = {
	name: name,
	version: version,
	description: description,
	main: main,
	module: module,
	repository: repository,
	author: author,
	license: license,
	"private": false,
	scripts: scripts,
	devDependencies: devDependencies,
	dependencies: dependencies,
	husky: husky,
	"lint-staged": {
	"*.js": [
		"eslint --fix",
		"git add"
	]
},
	files: files
};

const unusual = new Unusual(pkg.name + '@' + pkg.version);
const alphabet = 'abcdefghijklmnopqrstuvwxyz';
unusual.shuffle(
  `${alphabet}${alphabet.toString()}0123456789`.split('')
);

curry((bad, good, x) => fork(bad)(good)(x));

/**
 * takes an arity and nodeback function
 * and returns a future-returning function
 * NB: arity in this case doesn't take the callback into the count
 * so arity = 2 means a function with the shape (a, b, callback) => {}
 *
 * @function callbackToTheFuture
 * @param {number} arity - number of params
 * @param {Function} fn - a nodeback-style function
 * @returns {Function} Future-returning function
 * @example
 * import fs from "fs"
 * import { callbackToTheFuture, fork } from "torpor/utils"
 * import { trace } from "xtrace"
 * const readFile = callbackToTheFuture(2, fs.readFile)
 * fork(trace("bad"), trace("good"), readFile("myfile.txt", "utf8"))
 */
const callbackToTheFutureWithCancel = curry(
  (cancel, arity, fn) =>
    curryN(arity, function warp(...args) {
      return new Future((bad, good) => {
        const newArgs = [...args, (e, f) => (e ? bad(e) : good(f))];
        fn.apply(this, newArgs);
        return cancel
      })
    })
);
const callbackToTheFuture = callbackToTheFutureWithCancel(
  () => {}
);

/**
 * takes an arity and nodeback function
 * and returns a curried future-returning function
 * NB: arity in this case doesn't take the callback into the count
 * so arity = 2 means a function with the shape (a, b, callback) => {}
 *
 * @function proxifyN
 * @param {number} arity - arity
 * @param {Function} fn - a nodeback style function
 * @returns {Function} curried Future-returning function
 * @see callbackToTheFuture
 * @example
 * import fs from "fs"
 * import { proxifyN, fork } from "torpor/utils"
 * import { trace } from "xtrace"
 * import { pipe, map, __ as $ } from "ramda"
 * const readFile = proxifyN(2, fs.readFile)
 * const utf8 = readFile($, "utf8")
 * pipe(map(JSON.parse), fork(trace("bad"))(trace("good")))(utf8("package.json"))
 */
const proxifyN = curry((arity, fn) =>
  pipe(curryN(arity), callbackToTheFuture(arity))(fn)
);

const cbPassFailWithCancel = curry((cancel, arity, fn) =>
  curryN(arity, function passfail(...args) {
    return new Future((bad, good) => {
      const newArgs = [...args, e => (e ? bad(false) : good(true))];
      fn.apply(this, newArgs);
      return cancel
    })
  })
);

const cbPassFail = cbPassFailWithCancel(
  /* istanbul ignore next */
  () => {}
);

const readOrWriteWithCancel = curry(
  (cancel, fn, fd, buffer, offset, len, position) =>
    new Future((bad, good) => {
      fn(fd, buffer, offset, len, position, (e, bytes, buff) =>
        e ? bad(e) : good(bytes, buff)
      );
      return cancel
    })
);
const readOrWrite = readOrWriteWithCancel(
  /* istanbul ignore next */
  () => {}
);
const memo = memoizeWith(identity);
const within = curry((y, x) => ~x.indexOf(y));
const pInt = memo(z => parseInt(z, 10));
pipe(pInt, z => !isNaN(z));

memo(z =>
  z.substr(z.lastIndexOf(path.sep) + 1)
);
memo(z => z[z.length - 1] === `~`);
const UNKNOWN = `UNKNOWN`;
propOr(UNKNOWN);
curry((a, x) => x === a);
/*
{
  CREATED: 0,
  DELETED: 1,
  MODIFIED: 2,
  RENAMED: 3
}
*/
// export const isCreated = isAction(nsfw.actions.CREATED)
// export const isModified = isAction(nsfw.actions.MODIFIED)
// export const isDeleted = isAction(nsfw.actions.DELETED)
// export const isRenamed = isAction(nsfw.actions.RENAMED)
// export const getFile = curry((action, previous, current) =>
//   isDeleted(action)
//     ? { previous: current }
//     : previous && current
//     ? {
//         previous,
//         current
//       }
//     : current
//     ? { current }
//     : { previous }
// )

const existsAndIsWithin = curry((y, x) => x && within(y, x));

curry((where, y, x) =>
  pipe(where ? prop(where) : identity, existsAndIsWithin(y))(x)
);

const findF = curry((fn, def, x) =>
  pipe(
    map(fn),
    map(mapRej(() => false)),
    reduce((a, b) => (isFuture(a) ? race(a)(b) : b), def)
  )(x)
);

const [pf1, pf2, pf3] = map(cbPassFail, [1, 2, 3]);
const { constants } = fs;

const close$1 = pf1(fs.close);
const fdatasync$1 = pf1(fs.fdatasync);
const fsync$1 = pf1(fs.fsync);
const rmdir$1 = pf1(fs.rmdir);
const unlink$1 = pf1(fs.unlink);

/**
 * It's a TypedArray
 *
 * @typedef {Object} TypedArray
 */

/**
 * It's a Future!
 *
 * @typedef {Object} Future
 * @function fork
 * @function map
 * @function chain
 * @function swap
 * @function fold
 * @function bimap
 * @function mapRej
 * @function chainRej
 */

const access$1 = pf2(fs.access);
const chmod$1 = pf2(fs.chmod);
const fchmod$1 = pf2(fs.fchmod);
const fstat$1 = pf2(fs.fstat);
const ftruncate$1 = pf2(fs.ftruncate);
const lchmod$1 = pf2(fs.lchmod); // eslint-disable-line
const link$1 = pf2(fs.link);
const mkdir$1 = pf2(fs.mkdir);
const rename$1 = pf2(fs.rename);
const truncate$1 = pf2(fs.truncate);

const appendFile$1 = pf3(fs.appendFile);
const chown$1 = pf3(fs.chown);
const copyFile$1 = pf3(fs.copyFile);
const fchown$1 = pf3(fs.fchown);
const futimes$1 = pf3(fs.futimes);
const lchown$1 = pf3(fs.lchown); // eslint-disable-line
const symlink$1 = pf3(fs.symlink);
const utimes$1 = pf3(fs.utimes);
const writeFile$1 = pf3(fs.writeFile);

const createReadStream = proxifyN(2, fs.createReadStream);
const createWriteStream = proxifyN(2, fs.createWriteStream);
const lstat = proxifyN(2, fs.lstat);
const mkdtemp = proxifyN(2, fs.mkdtemp);
const readFile = proxifyN(2, fs.readFile);
const readdir = proxifyN(2, fs.readdir);
const readlink = proxifyN(2, fs.readlink);
const realpath = proxifyN(2, fs.realpath);
const stat = proxifyN(2, fs.stat);

const open = proxifyN(3, fs.open);

const read = readOrWrite(fs.read);
const write = readOrWrite(fs.write);

/* eslint-disable max-len */
// these are autogenerated with read !node derived/get-documentation.js fs.close
/*

read ! cat src/fs.js| snang -P "split(C.n) | reject(matches('cat')) | filter(matches('pf')) | tail | map(split(C._)) | map(find(matches('pf'))) | map(z => z.slice(4, z.length - 1)) | join(C._) | z => 'node derived/get-documentation-for.js \'' + z + '\''" -o

read !node derived/get-documentation-for.js 'fs.close fs.fdatasync fs.fsync fs.rmdir fs.unlink fs.access fs.chmod fs.fchmod fs.fstat fs.lchmod fs.link fs.mkdir fs.rename fs.appendFile fs.chown fs.copyFile fs.fchown fs.futimes fs.lchown fs.symlink fs.utimes fs.writeFile'
*/

/**
 * Wraps [fs.close](https://nodejs.org/api/fs.html#fs_fs_close_fd_callback)
 * with [fluture](https://github.com/fluture-js/Fluture) so that instead of taking a
 * callback it returns a Future. Unlike the raw function it wraps, this function now returns a
 * Future which wraps a value indicating whether the operation succeeded.
 *
 * via [node API docs](https://nodejs.org/api/fs.html):
 *
 * > Asynchronous [`close(2)`](http://man7.org/linux/man-pages/man2/close.2.html).
 *
 * > No arguments other than a possible exception are given
 * > to the completion callback.
 *
 * @function close
 * @public
 * @param {number} fd -
 * @returns {Future} - a future value
 * @see {@link https://github.com/nodejs/node/blob/e6872f5f4bf7dc418bd77a3f2a911fb2c48e3df1/lib/fs.js#L398}
 * for the source function this function wraps.
 */
/**
 * Wraps [fs.fdatasync](https://nodejs.org/api/fs.html#fs_fs_fdatasync_fd_callback)
 * with [fluture](https://github.com/fluture-js/Fluture) so that instead of taking a
 * callback it returns a Future. Unlike the raw function it wraps, this function now returns a
 * Future which wraps a value indicating whether the operation succeeded.
 *
 * via [node API docs](https://nodejs.org/api/fs.html):
 *
 * > Asynchronous [`fdatasync(2)`](http://man7.org/linux/man-pages/man2/fdatasync.2.html).
 *
 * > No arguments other than a possible exception are
 * > given to the completion callback.
 *
 * @function fdatasync
 * @public
 * @param {number} fd -
 * @returns {Future} - a future value
 * @see {@link https://github.com/nodejs/node/blob/e6872f5f4bf7dc418bd77a3f2a911fb2c48e3df1/lib/fs.js#L738}
 * for the source function this function wraps.
 */
/**
 * Wraps [fs.fsync](https://nodejs.org/api/fs.html#fs_fs_fsync_fd_callback)
 * with [fluture](https://github.com/fluture-js/Fluture) so that instead of taking a
 * callback it returns a Future. Unlike the raw function it wraps, this function now returns a
 * Future which wraps a value indicating whether the operation succeeded.
 *
 * via [node API docs](https://nodejs.org/api/fs.html):
 *
 * > Asynchronous [`fsync(2)`](http://man7.org/linux/man-pages/man2/fsync.2.html).
 *
 * > No arguments other than a possible exception are given
 * > to the completion callback.
 *
 * @function fsync
 * @public
 * @param {number} fd -
 * @returns {Future} - a future value
 * @see {@link https://github.com/nodejs/node/blob/e6872f5f4bf7dc418bd77a3f2a911fb2c48e3df1/lib/fs.js#L752}
 * for the source function this function wraps.
 */
/**
 * Wraps [fs.rmdir](https://nodejs.org/api/fs.html#fs_fs_rmdir_path_callback)
 * with [fluture](https://github.com/fluture-js/Fluture) so that instead of taking a
 * callback it returns a Future. Unlike the raw function it wraps, this function now returns a
 * Future which wraps a value indicating whether the operation succeeded.
 *
 * via [node API docs](https://nodejs.org/api/fs.html):
 *
 * > Asynchronous [`rmdir(2)`](http://man7.org/linux/man-pages/man2/rmdir.2.html).
 *
 * > No arguments other than a possible exception are given
 * > to the completion callback.
 *
 * > Using `fs.rmdir()` on a file (not a directory) results in an `ENOENT` error on
 * > Windows and an `ENOTDIR` error on POSIX.
 *
 * @function rmdir
 * @public
 * @param {string|Buffer|URL} path -
 * @returns {Future} - a future value
 * @see {@link https://github.com/nodejs/node/blob/e6872f5f4bf7dc418bd77a3f2a911fb2c48e3df1/lib/fs.js#L723}
 * for the source function this function wraps.
 */
/**
 * Wraps [fs.unlink](https://nodejs.org/api/fs.html#fs_fs_unlink_path_callback)
 * with [fluture](https://github.com/fluture-js/Fluture) so that instead of taking a
 * callback it returns a Future. Unlike the raw function it wraps, this function now returns a
 * Future which wraps a value indicating whether the operation succeeded.
 *
 * via [node API docs](https://nodejs.org/api/fs.html):
 *
 * > Asynchronously removes a file or symbolic link.
 *
 * > No arguments other than a
 * > possible exception are given to the completion callback.
 *
 * >     `// Assuming that 'path/file.txt' is a regular file.
 * >     fs.unlink('path/file.txt', (err) => {
 * >     if (err) throw err;
 * >     console.log('path/file.txt was deleted');
 * >     });
 * >     `.
 *
 * > `fs.unlink()` will not work on a directory, empty or otherwise.
 *
 * > To remove a
 * > directory, use [`fs.rmdir()`](#fs_fs_rmdir_path_callback).
 *
 * > See also: [`unlink(2)`](http://man7.org/linux/man-pages/man2/unlink.2.html).
 *
 * @function unlink
 * @public
 * @param {string|Buffer|URL} path -
 * @returns {Future} - a future value
 * @see {@link https://github.com/nodejs/node/blob/e6872f5f4bf7dc418bd77a3f2a911fb2c48e3df1/lib/fs.js#L1006}
 * for the source function this function wraps.
 */
/**
 * Wraps [fs.access](https://nodejs.org/api/fs.html#fs_fs_access_path_mode_callback)
 * with [fluture](https://github.com/fluture-js/Fluture) so that instead of taking a
 * callback it returns a Future. Unlike the raw function it wraps, this function now returns a
 * Future which wraps a value indicating whether the operation succeeded.
 *
 * via [node API docs](https://nodejs.org/api/fs.html):
 *
 * > Tests a user's permissions for the file or directory specified by `path`.
 * > The `mode` argument is an optional number that specifies the accessibility
 * > checks to be performed.
 *
 * > Check [File Access Constants](#fs_file_access_constants) for possible values
 * > of `mode`.
 *
 * > It is possible to create a mask consisting of the bitwise OR of
 * > two or more values (e.g.
 *
 * > `fs.constants.W_OK | fs.constants.R_OK`).
 *
 * > The final argument, `callback`, is a callback function that is invoked with
 * > a possible error argument.
 *
 * > If any of the accessibility checks fail, the error
 * > argument will be an `Error` object.
 *
 * > The following examples check if
 * > `package.json` exists, and if it is readable or writable.
 *
 * >     `const file = 'package.json';
 * >
 * >     // Check if the file exists in the current directory.
 * >     fs.access(file, fs.constants.F_OK, (err) => {
 * >     console.log(\`${file} ${err ? 'does not exist' : 'exists'}\`);
 * >     });
 * >
 * >     // Check if the file is readable.
 * >     fs.access(file, fs.constants.R_OK, (err) => {
 * >     console.log(\`${file} ${err ? 'is not readable' : 'is readable'}\`);
 * >     });
 * >
 * >     // Check if the file is writable.
 * >     fs.access(file, fs.constants.W_OK, (err) => {
 * >     console.log(\`${file} ${err ? 'is not writable' : 'is writable'}\`);
 * >     });
 * >
 * >     // Check if the file exists in the current directory, and if it is writable.
 * >     fs.access(file, fs.constants.F_OK | fs.constants.W_OK, (err) => {
 * >     if (err) {
 * >     console.error(
 * >     \`${file} ${err.code === 'ENOENT' ? 'does not exist' : 'is read-only'}\`);
 * >     } else {
 * >     console.log(\`${file} exists, and it is writable\`);
 * >     }
 * >     });
 * >     `.
 *
 * > Using `fs.access()` to check for the accessibility of a file before calling
 * > `fs.open()`, `fs.readFile()` or `fs.writeFile()` is not recommended.
 *
 * > Doing
 * > so introduces a race condition, since other processes may change the file's
 * > state between the two calls.
 *
 * > Instead, user code should open/read/write the
 * > file directly and handle the error raised if the file is not accessible.
 *
 * >     `fs.access('myfile', (err) => {
 * >     if (!err) {
 * >     console.error('myfile already exists');
 * >     return;
 * >     }
 * >
 * >     fs.open('myfile', 'wx', (err, fd) => {
 * >     if (err) throw err;
 * >     writeMyData(fd);
 * >     });
 * >     });
 * >     `.
 *
 * >     `fs.open('myfile', 'wx', (err, fd) => {
 * >     if (err) {
 * >     if (err.code === 'EEXIST') {
 * >     console.error('myfile already exists');
 * >     return;
 * >     }
 * >
 * >     throw err;
 * >     }
 * >
 * >     writeMyData(fd);
 * >     });
 * >     `.
 *
 * >     `fs.access('myfile', (err) => {
 * >     if (err) {
 * >     if (err.code === 'ENOENT') {
 * >     console.error('myfile does not exist');
 * >     return;
 * >     }
 * >
 * >     throw err;
 * >     }
 * >
 * >     fs.open('myfile', 'r', (err, fd) => {
 * >     if (err) throw err;
 * >     readMyData(fd);
 * >     });
 * >     });
 * >     `.
 *
 * >     `fs.open('myfile', 'r', (err, fd) => {
 * >     if (err) {
 * >     if (err.code === 'ENOENT') {
 * >     console.error('myfile does not exist');
 * >     return;
 * >     }
 * >
 * >     throw err;
 * >     }
 * >
 * >     readMyData(fd);
 * >     });
 * >     `.
 *
 * > The "not recommended" examples above check for accessibility and then use the
 * > file; the "recommended" examples are better because they use the file directly
 * > and handle the error, if any.
 *
 * > In general, check for the accessibility of a file only if the file will not be
 * > used directly, for example when its accessibility is a signal from another
 * > process.
 *
 * > On Windows, access-control policies (ACLs) on a directory may limit access to
 * > a file or directory.
 *
 * > The `fs.access()` function, however, does not check the
 * > ACL and therefore may report that a path is accessible even if the ACL restricts
 * > the user from reading or writing to it.
 *
 * @function access
 * @public
 * @param {string|Buffer|URL} path -
 * @param {number} mode - Default: fs.constants.F_OK
 * @returns {Future} - a future value
 * @see {@link https://github.com/nodejs/node/blob/e6872f5f4bf7dc418bd77a3f2a911fb2c48e3df1/lib/fs.js#L179}
 * for the source function this function wraps.
 */
/**
 * Wraps [fs.chmod](https://nodejs.org/api/fs.html#fs_fs_chmod_path_mode_callback)
 * with [fluture](https://github.com/fluture-js/Fluture) so that instead of taking a
 * callback it returns a Future. Unlike the raw function it wraps, this function now returns a
 * Future which wraps a value indicating whether the operation succeeded.
 *
 * via [node API docs](https://nodejs.org/api/fs.html):
 *
 * > Asynchronously changes the permissions of a file.
 *
 * > No arguments other than a
 * > possible exception are given to the completion callback.
 *
 * > See also: [`chmod(2)`](http://man7.org/linux/man-pages/man2/chmod.2.html).
 *
 * >     `fs.chmod('my_file.txt', 0o775, (err) => {
 * >     if (err) throw err;
 * >     console.log('The permissions for file "my_file.txt" have been changed!');
 * >     });
 * >     `.
 *
 * > The `mode` argument used in both the `fs.chmod()` and `fs.chmodSync()`
 * > methods is a numeric bitmask created using a logical OR of the following
 * > constants:
 * > ConstantOctalDescription`fs.constants.S_IRUSR``0o400`read by owner`fs.constants.S_IWUSR``0o200`write by owner`fs.constants.S_IXUSR``0o100`execute/search by owner`fs.constants.S_IRGRP``0o40`read by group`fs.constants.S_IWGRP``0o20`write by group`fs.constants.S_IXGRP``0o10`execute/search by group`fs.constants.S_IROTH``0o4`read by others`fs.constants.S_IWOTH``0o2`write by others`fs.constants.S_IXOTH``0o1`execute/search by others.
 *
 * > An easier method of constructing the `mode` is to use a sequence of three
 * > octal digits (e.g.
 *
 * > `765`).
 *
 * > The left-most digit (`7` in the example), specifies
 * > the permissions for the file owner.
 *
 * > The middle digit (`6` in the example),
 * > specifies permissions for the group.
 *
 * > The right-most digit (`5` in the example),
 * > specifies the permissions for others.
 * > NumberDescription`7`read, write, and execute`6`read and write`5`read and execute`4`read only`3`write and execute`2`write only`1`execute only`0`no permission.
 *
 * > For example, the octal value `0o765` means:
 *
 * > When using raw numbers where file modes are expected, any value larger than
 * > `0o777` may result in platform-specific behaviors that are not supported to work
 * > consistently.
 *
 * > Therefore constants like `S_ISVTX`, `S_ISGID` or `S_ISUID` are not
 * > exposed in `fs.constants`.
 *
 * > Caveats: on Windows only the write permission can be changed, and the
 * > distinction among the permissions of group, owner or others is not
 * > implemented.
 *
 * @function chmod
 * @public
 * @param {string|Buffer|URL} path -
 * @param {number} mode -
 * @param The - owner may read, write and execute the file
 * @param The - group may read and write the file
 * @param Others - may read and execute the file
 * @returns {Future} - a future value
 * @see {@link https://github.com/nodejs/node/blob/e6872f5f4bf7dc418bd77a3f2a911fb2c48e3df1/lib/fs.js#L1071}
 * for the source function this function wraps.
 */
/**
 * Wraps [fs.fchmod](https://nodejs.org/api/fs.html#fs_fs_fchmod_fd_mode_callback)
 * with [fluture](https://github.com/fluture-js/Fluture) so that instead of taking a
 * callback it returns a Future. Unlike the raw function it wraps, this function now returns a
 * Future which wraps a value indicating whether the operation succeeded.
 *
 * via [node API docs](https://nodejs.org/api/fs.html):
 *
 * > Asynchronous [`fchmod(2)`](http://man7.org/linux/man-pages/man2/fchmod.2.html).
 *
 * > No arguments other than a possible exception
 * > are given to the completion callback.
 *
 * @function fchmod
 * @public
 * @param {number} fd -
 * @param {number} mode -
 * @returns {Future} - a future value
 * @see {@link https://github.com/nodejs/node/blob/e6872f5f4bf7dc418bd77a3f2a911fb2c48e3df1/lib/fs.js#L1021}
 * for the source function this function wraps.
 */
/**
 * Wraps [fs.fstat](https://nodejs.org/api/fs.html#fs_fs_fstat_fd_options_callback)
 * with [fluture](https://github.com/fluture-js/Fluture) so that instead of taking a
 * callback it returns a Future. Unlike the raw function it wraps, this function now returns a
 * Future which wraps a value indicating whether the operation succeeded.
 *
 * via [node API docs](https://nodejs.org/api/fs.html):
 *
 * > Asynchronous [`fstat(2)`](http://man7.org/linux/man-pages/man2/fstat.2.html).
 *
 * > The callback gets two arguments `(err, stats)` where
 * > `stats` is an [`fs.Stats`](#fs_class_fs_stats) object.
 *
 * > `fstat()` is identical to [`stat()`](fs.html#fs_fs_stat_path_options_callback),
 * > except that the file to be stat-ed is specified by the file descriptor `fd`.
 *
 * @function fstat
 * @public
 * @param {number} fd -
 * @param {Object} options -
 * @param {boolean} options.bigint - Whether the numeric values in the returned
 * @returns {Future} - a future value
 * @see {@link https://github.com/nodejs/node/blob/e6872f5f4bf7dc418bd77a3f2a911fb2c48e3df1/lib/fs.js#L841}
 * for the source function this function wraps.
 */
/**
 * Wraps [fs.lchmod](https://nodejs.org/api/fs.html#fs_fs_lchmod_path_mode_callback)
 * with [fluture](https://github.com/fluture-js/Fluture) so that instead of taking a
 * callback it returns a Future. Unlike the raw function it wraps, this function now returns a
 * Future which wraps a value indicating whether the operation succeeded.
 *
 * via [node API docs](https://nodejs.org/api/fs.html):
 *
 * > Asynchronous [`lchmod(2)`](https://www.freebsd.org/cgi/man.cgi?query=lchmod&sektion=2).
 *
 * > No arguments other than a possible exception
 * > are given to the completion callback.
 *
 * > Only available on macOS.
 *
 * @function lchmod
 * @public
 * @param {string|Buffer|URL} path -
 * @param {number} mode -
 * @returns {Future} - a future value
 *
 */
/**
 * Wraps [fs.link](https://nodejs.org/api/fs.html#fs_fs_link_existingpath_newpath_callback)
 * with [fluture](https://github.com/fluture-js/Fluture) so that instead of taking a
 * callback it returns a Future. Unlike the raw function it wraps, this function now returns a
 * Future which wraps a value indicating whether the operation succeeded.
 *
 * via [node API docs](https://nodejs.org/api/fs.html):
 *
 * > Asynchronous [`link(2)`](http://man7.org/linux/man-pages/man2/link.2.html).
 *
 * > No arguments other than a possible exception are given to
 * > the completion callback.
 *
 * @function link
 * @public
 * @param {string|Buffer|URL} existingPath -
 * @param {string|Buffer|URL} newPath -
 * @returns {Future} - a future value
 * @see {@link https://github.com/nodejs/node/blob/e6872f5f4bf7dc418bd77a3f2a911fb2c48e3df1/lib/fs.js#L980}
 * for the source function this function wraps.
 */
/**
 * Wraps [fs.mkdir](https://nodejs.org/api/fs.html#fs_fs_mkdir_path_options_callback)
 * with [fluture](https://github.com/fluture-js/Fluture) so that instead of taking a
 * callback it returns a Future. Unlike the raw function it wraps, this function now returns a
 * Future which wraps a value indicating whether the operation succeeded.
 *
 * via [node API docs](https://nodejs.org/api/fs.html):
 *
 * > Asynchronously creates a directory.
 *
 * > No arguments other than a possible exception
 * > are given to the completion callback.
 *
 * > The optional `options` argument can be an number specifying mode (permission
 * > and sticky bits), or an object with a `mode` property and a `recursive`
 * > property indicating whether parent folders should be created.
 *
 * > Calling
 * > `fs.mkdir()` when `path` is a directory that exists results in an error only
 * > when `recursive` is false.
 *
 * >     `// Creates /tmp/a/apple, regardless of whether \`/tmp\` and /tmp/a exist.
 * >     fs.mkdir('/tmp/a/apple', { recursive: true }, (err) => {
 * >     if (err) throw err;
 * >     });
 * >     `.
 *
 * > On Windows, using `fs.mkdir()` on the root directory even with recursion will
 * > result in an error:
 *
 * >     `fs.mkdir('/', { recursive: true }, (err) => {
 * >     // => [Error: EPERM: operation not permitted, mkdir 'C:\']
 * >     });
 * >     `.
 *
 * > See also: [`mkdir(2)`](http://man7.org/linux/man-pages/man2/mkdir.2.html).
 *
 * @function mkdir
 * @public
 * @param {string|Buffer|URL} path -
 * @param {Object|number} options -
 * @param {boolean} options.recursive - Default: false
 * @param {number} options.mode - Not supported on Windows Default: 0o777
 * @returns {Future} - a future value
 * @see {@link https://github.com/nodejs/node/blob/e6872f5f4bf7dc418bd77a3f2a911fb2c48e3df1/lib/fs.js#L766}
 * for the source function this function wraps.
 */
/**
 * Wraps [fs.rename](https://nodejs.org/api/fs.html#fs_fs_rename_oldpath_newpath_callback)
 * with [fluture](https://github.com/fluture-js/Fluture) so that instead of taking a
 * callback it returns a Future. Unlike the raw function it wraps, this function now returns a
 * Future which wraps a value indicating whether the operation succeeded.
 *
 * via [node API docs](https://nodejs.org/api/fs.html):
 *
 * > Asynchronously rename file at `oldPath` to the pathname provided
 * > as `newPath`.
 *
 * > In the case that `newPath` already exists, it will
 * > be overwritten.
 *
 * > If there is a directory at `newPath`, an error will
 * > be raised instead.
 *
 * > No arguments other than a possible exception are
 * > given to the completion callback.
 *
 * > See also: [`rename(2)`](http://man7.org/linux/man-pages/man2/rename.2.html).
 *
 * >     `fs.rename('oldFile.txt', 'newFile.txt', (err) => {
 * >     if (err) throw err;
 * >     console.log('Rename complete!');
 * >     });
 * >     `.
 *
 * @function rename
 * @public
 * @param {string|Buffer|URL} oldPath -
 * @param {string|Buffer|URL} newPath -
 * @returns {Future} - a future value
 * @see {@link https://github.com/nodejs/node/blob/e6872f5f4bf7dc418bd77a3f2a911fb2c48e3df1/lib/fs.js#L634}
 * for the source function this function wraps.
 */
/**
 * Wraps [fs.appendFile](https://nodejs.org/api/fs.html#fs_fs_appendfile_path_data_options_callback)
 * with [fluture](https://github.com/fluture-js/Fluture) so that instead of taking a
 * callback it returns a Future. Unlike the raw function it wraps, this function now returns a
 * Future which wraps a value indicating whether the operation succeeded.
 *
 * via [node API docs](https://nodejs.org/api/fs.html):
 *
 * > Asynchronously append data to a file, creating the file if it does not yet
 * > exist.
 *
 * > `data` can be a string or a [`Buffer`](buffer.html#buffer_buffer).
 *
 * >     `fs.appendFile('message.txt', 'data to append', (err) => {
 * >     if (err) throw err;
 * >     console.log('The "data to append" was appended to file!');
 * >     });
 * >     `.
 *
 * > If `options` is a string, then it specifies the encoding:
 *
 * >     `fs.appendFile('message.txt', 'data to append', 'utf8', callback);
 * >     `
 *
 * > The `path` may be specified as a numeric file descriptor that has been opened
 * > for appending (using `fs.open()` or `fs.openSync()`).
 *
 * > The file descriptor will
 * > not be closed automatically.
 *
 * >     `fs.open('message.txt', 'a', (err, fd) => {
 * >     if (err) throw err;
 * >     fs.appendFile(fd, 'data to append', 'utf8', (err) => {
 * >     fs.close(fd, (err) => {
 * >     if (err) throw err;
 * >     });
 * >     if (err) throw err;
 * >     });
 * >     });
 * >     `.
 *
 * @function appendFile
 * @public
 * @param {string|Buffer|URL|number} path - filename or file descriptor
 * @param {string|Buffer} data -
 * @param {Object|string} options -
 * @param {string|null} options.encoding - Default: 'utf8'
 * @param {number} options.mode - Default: 0o666
 * @param {string} options.flag - See {support of file system flags} - Default: 'a'
 * @returns {Future} - a future value
 * @see {@link https://github.com/nodejs/node/blob/e6872f5f4bf7dc418bd77a3f2a911fb2c48e3df1/lib/fs.js#L1270}
 * for the source function this function wraps.
 */
/**
 * Wraps [fs.chown](https://nodejs.org/api/fs.html#fs_fs_chown_path_uid_gid_callback)
 * with [fluture](https://github.com/fluture-js/Fluture) so that instead of taking a
 * callback it returns a Future. Unlike the raw function it wraps, this function now returns a
 * Future which wraps a value indicating whether the operation succeeded.
 *
 * via [node API docs](https://nodejs.org/api/fs.html):
 *
 * > Asynchronously changes owner and group of a file.
 *
 * > No arguments other than a
 * > possible exception are given to the completion callback.
 *
 * > See also: [`chown(2)`](http://man7.org/linux/man-pages/man2/chown.2.html).
 *
 * @function chown
 * @public
 * @param {string|Buffer|URL} path -
 * @param {number} uid -
 * @param {number} gid -
 * @returns {Future} - a future value
 * @see {@link https://github.com/nodejs/node/blob/e6872f5f4bf7dc418bd77a3f2a911fb2c48e3df1/lib/fs.js#L1129}
 * for the source function this function wraps.
 */
/**
 * Wraps [fs.copyFile](https://nodejs.org/api/fs.html#fs_fs_copyfile_src_dest_flags_callback)
 * with [fluture](https://github.com/fluture-js/Fluture) so that instead of taking a
 * callback it returns a Future. Unlike the raw function it wraps, this function now returns a
 * Future which wraps a value indicating whether the operation succeeded.
 *
 * via [node API docs](https://nodejs.org/api/fs.html):
 *
 * > Added in: v8.5.0
 *
 * > Asynchronously copies `src` to `dest`.
 *
 * > By default, `dest` is overwritten if it
 * > already exists.
 *
 * > No arguments other than a possible exception are given to the
 * > callback function.
 *
 * > Node.js makes no guarantees about the atomicity of the copy
 * > operation.
 *
 * > If an error occurs after the destination file has been opened for
 * > writing, Node.js will attempt to remove the destination.
 *
 * > `flags` is an optional number that specifies the behavior
 * > of the copy operation.
 *
 * > It is possible to create a mask consisting of the bitwise
 * > OR of two or more values (e.g.
 * > `fs.constants.COPYFILE_EXCL | fs.constants.COPYFILE_FICLONE`).
 *
 * >     `const fs = require('fs');
 * >
 * >     // destination.txt will be created or overwritten by default.
 * >     fs.copyFile('source.txt', 'destination.txt', (err) => {
 * >     if (err) throw err;
 * >     console.log('source.txt was copied to destination.txt');
 * >     });
 * >     `.
 *
 * > If the third argument is a number, then it specifies `flags`:
 *
 * >     `const fs = require('fs');
 * >     const { COPYFILE_EXCL } = fs.constants;
 * >
 * >     // By using COPYFILE_EXCL, the operation will fail if destination.txt exists.
 * >     fs.copyFile('source.txt', 'destination.txt', COPYFILE_EXCL, callback);
 * >     `.
 *
 * @function copyFile
 * @public
 * @param {string|Buffer|URL} src - source filename to copy
 * @param {string|Buffer|URL} dest - destination filename of the copy operation
 * @param {number} flags - modifiers for copy operation Default: 0
 * @returns {Future} - a future value
 * @see {@link https://github.com/nodejs/node/blob/e6872f5f4bf7dc418bd77a3f2a911fb2c48e3df1/lib/fs.js#L1743}
 * for the source function this function wraps.
 */
/**
 * Wraps [fs.fchown](https://nodejs.org/api/fs.html#fs_fs_fchown_fd_uid_gid_callback)
 * with [fluture](https://github.com/fluture-js/Fluture) so that instead of taking a
 * callback it returns a Future. Unlike the raw function it wraps, this function now returns a
 * Future which wraps a value indicating whether the operation succeeded.
 *
 * via [node API docs](https://nodejs.org/api/fs.html):
 *
 * > Asynchronous [`fchown(2)`](http://man7.org/linux/man-pages/man2/fchown.2.html).
 *
 * > No arguments other than a possible exception are given
 * > to the completion callback.
 *
 * @function fchown
 * @public
 * @param {number} fd -
 * @param {number} uid -
 * @param {number} gid -
 * @returns {Future} - a future value
 * @see {@link https://github.com/nodejs/node/blob/e6872f5f4bf7dc418bd77a3f2a911fb2c48e3df1/lib/fs.js#L1109}
 * for the source function this function wraps.
 */
/**
 * Wraps [fs.futimes](https://nodejs.org/api/fs.html#fs_fs_futimes_fd_atime_mtime_callback)
 * with [fluture](https://github.com/fluture-js/Fluture) so that instead of taking a
 * callback it returns a Future. Unlike the raw function it wraps, this function now returns a
 * Future which wraps a value indicating whether the operation succeeded.
 *
 * via [node API docs](https://nodejs.org/api/fs.html):
 *
 * > Change the file system timestamps of the object referenced by the supplied file
 * > descriptor.
 *
 * > See [`fs.utimes()`](#fs_fs_utimes_path_atime_mtime_callback).
 *
 * > This function does not work on AIX versions before 7.1, it will return the
 * > error `UV_ENOSYS`.
 *
 * @function futimes
 * @public
 * @param {number} fd -
 * @param {number|string|Date} atime -
 * @param {number|string|Date} mtime -
 * @returns {Future} - a future value
 * @see {@link https://github.com/nodejs/node/blob/e6872f5f4bf7dc418bd77a3f2a911fb2c48e3df1/lib/fs.js#L1170}
 * for the source function this function wraps.
 */
/**
 * Wraps [fs.lchown](https://nodejs.org/api/fs.html#fs_fs_lchown_path_uid_gid_callback)
 * with [fluture](https://github.com/fluture-js/Fluture) so that instead of taking a
 * callback it returns a Future. Unlike the raw function it wraps, this function now returns a
 * Future which wraps a value indicating whether the operation succeeded.
 *
 * via [node API docs](https://nodejs.org/api/fs.html):
 *
 * > Asynchronous [`lchown(2)`](http://man7.org/linux/man-pages/man2/lchown.2.html).
 *
 * > No arguments other than a possible exception are given
 * > to the completion callback.
 *
 * @function lchown
 * @public
 * @param {string|Buffer|URL} path -
 * @param {number} uid -
 * @param {number} gid -
 * @returns {Future} - a future value
 * @see {@link https://github.com/nodejs/node/blob/e6872f5f4bf7dc418bd77a3f2a911fb2c48e3df1/lib/fs.js#L1090}
 * for the source function this function wraps.
 */
/**
 * Wraps [fs.symlink](https://nodejs.org/api/fs.html#fs_fs_symlink_target_path_type_callback)
 * with [fluture](https://github.com/fluture-js/Fluture) so that instead of taking a
 * callback it returns a Future. Unlike the raw function it wraps, this function now returns a
 * Future which wraps a value indicating whether the operation succeeded.
 *
 * via [node API docs](https://nodejs.org/api/fs.html):
 *
 * > Asynchronous [`symlink(2)`](http://man7.org/linux/man-pages/man2/symlink.2.html).
 *
 * > No arguments other than a possible exception are given
 * > to the completion callback.
 *
 * > The `type` argument is only available on Windows
 * > and ignored on other platforms.
 *
 * > It can be set to `'dir'`, `'file'`, or
 * > `'junction'`.
 *
 * > If the `type` argument is not set, Node will autodetect `target`
 * > type and use `'file'` or `'dir'`.
 *
 * > If the `target` does not exist, `'file'` will
 * > be used.
 *
 * > Windows junction points require the destination path to be absolute.
 * > When using `'junction'`, the `target` argument will automatically be normalized
 * > to absolute path.
 *
 * > Here is an example below:
 *
 * >     `fs.symlink('./foo', './new-port', callback);
 * >     `
 *
 * > It creates a symbolic link named "new-port" that points to "foo".
 *
 * @function symlink
 * @public
 * @param {string|Buffer|URL} target -
 * @param {string|Buffer|URL} path -
 * @param {string} type -
 * @returns {Future} - a future value
 * @see {@link https://github.com/nodejs/node/blob/e6872f5f4bf7dc418bd77a3f2a911fb2c48e3df1/lib/fs.js#L921}
 * for the source function this function wraps.
 */
/**
 * Wraps [fs.utimes](https://nodejs.org/api/fs.html#fs_fs_utimes_path_atime_mtime_callback)
 * with [fluture](https://github.com/fluture-js/Fluture) so that instead of taking a
 * callback it returns a Future. Unlike the raw function it wraps, this function now returns a
 * Future which wraps a value indicating whether the operation succeeded.
 *
 * via [node API docs](https://nodejs.org/api/fs.html):
 *
 * > Change the file system timestamps of the object referenced by `path`.
 *
 * > The `atime` and `mtime` arguments follow these rules:
 *
 * @function utimes
 * @public
 * @param {string|Buffer|URL} path -
 * @param {number|string|Date} atime -
 * @param {number|string|Date} mtime -
 * @param Values - can be either numbers representing Unix epoch time, Dates, or a
 * @param If - the value can not be converted to a number, or is NaN, Infinity or
 * @returns {Future} - a future value
 * @see {@link https://github.com/nodejs/node/blob/e6872f5f4bf7dc418bd77a3f2a911fb2c48e3df1/lib/fs.js#L1149}
 * for the source function this function wraps.
 */
/**
 * Wraps [fs.writeFile](https://nodejs.org/api/fs.html#fs_fs_writefile_file_data_options_callback)
 * with [fluture](https://github.com/fluture-js/Fluture) so that instead of taking a
 * callback it returns a Future. Unlike the raw function it wraps, this function now returns a
 * Future which wraps a value indicating whether the operation succeeded.
 *
 * via [node API docs](https://nodejs.org/api/fs.html):
 *
 * > When `file` is a filename, asynchronously writes data to the file, replacing the
 * > file if it already exists.
 *
 * > `data` can be a string or a buffer.
 *
 * > When `file` is a file descriptor, the behavior is similar to calling
 * > `fs.write()` directly (which is recommended).
 *
 * > See the notes below on using
 * > a file descriptor.
 *
 * > The `encoding` option is ignored if `data` is a buffer.
 *
 * >     `const data = new Uint8Array(Buffer.from('Hello Node.js'));
 * >     fs.writeFile('message.txt', data, (err) => {
 * >     if (err) throw err;
 * >     console.log('The file has been saved!');
 * >     });
 * >     `.
 *
 * > If `options` is a string, then it specifies the encoding:
 *
 * >     `fs.writeFile('message.txt', 'Hello Node.js', 'utf8', callback);
 * >     `
 *
 * > It is unsafe to use `fs.writeFile()` multiple times on the same file without
 * > waiting for the callback.
 *
 * > For this scenario, [`fs.createWriteStream()`](#fs_fs_createwritestream_path_options) is
 * > recommended.
 *
 * > When `file` is a file descriptor, the behavior is almost identical to directly
 * > calling `fs.write()` like:.
 *
 * >     `fs.write(fd, Buffer.from(data, options.encoding), callback);
 * >     `
 *
 * > The difference from directly calling `fs.write()` is that under some unusual
 * > conditions, `fs.write()` may write only part of the buffer and will need to be
 * > retried to write the remaining data, whereas `fs.writeFile()` will retry until
 * > the data is entirely written (or an error occurs).
 *
 * > The implications of this are a common source of confusion.
 *
 * > In
 * > the file descriptor case, the file is not replaced! The data is not necessarily
 * > written to the beginning of the file, and the file's original data may remain
 * > before and/or after the newly written data.
 *
 * > For example, if `fs.writeFile()` is called twice in a row, first to write the
 * > string `'Hello'`, then to write the string `', World'`, the file would contain
 * > `'Hello, World'`, and might contain some of the file's original data (depending
 * > on the size of the original file, and the position of the file descriptor).
 *
 * > If
 * > a file name had been used instead of a descriptor, the file would be guaranteed
 * > to contain only `', World'`.
 *
 * @function writeFile
 * @public
 * @param {string|Buffer|URL|number} file - filename or file descriptor
 * @param {string|Buffer|TypedArray|DataView} data -
 * @param {Object|string} options -
 * @param {string|null} options.encoding - Default: 'utf8'
 * @param {number} options.mode - Default: 0o666
 * @param {string} options.flag - See {support of file system flags} - Default: 'w'
 * @returns {Future} - a future value
 * @see {@link https://github.com/nodejs/node/blob/e6872f5f4bf7dc418bd77a3f2a911fb2c48e3df1/lib/fs.js#L1216}
 * for the source function this function wraps.
 */

/* eslint-enable max-len */

const para1 = fn => pipe(fn, Par.of);
const para2 = curry(
  /* istanbul ignore next */
  (fn, x, y) => pipe(fn(x), Par.of)(y)
);

const para3 = curry(
  /* istanbul ignore next */
  (fn, x, y, z) => pipe(fn(x, y), Par.of)(z)
);

const close = para1(close$1);
const fdatasync = para1(fdatasync$1);
const fsync = para1(fsync$1);
const rmdir = para1(rmdir$1);
const unlink = para1(unlink$1);

const access = para2(access$1);
const chmod = para2(chmod$1);
const fchmod = para2(fchmod$1);
const fstat = para2(fstat$1);
const ftruncate = para2(ftruncate$1);
const lchmod = para2(lchmod$1);
const link = para2(link$1);
const mkdir = para2(mkdir$1);
const rename = para2(rename$1);
const truncate = para2(truncate$1);

const appendFile = para3(appendFile$1);
const chown = para3(chown$1);
const copyFile = para3(copyFile$1);
const fchown = para3(fchown$1);
const futimes = para3(futimes$1);
const lchown = para3(lchown$1);
const symlink = para3(symlink$1);
const utimes = para3(utimes$1);
const writeFile = para3(writeFile$1);

var fsParallel = /*#__PURE__*/Object.freeze({
  __proto__: null,
  close: close,
  fdatasync: fdatasync,
  fsync: fsync,
  rmdir: rmdir,
  unlink: unlink,
  access: access,
  chmod: chmod,
  fchmod: fchmod,
  fstat: fstat,
  ftruncate: ftruncate,
  lchmod: lchmod,
  link: link,
  mkdir: mkdir,
  rename: rename,
  truncate: truncate,
  appendFile: appendFile,
  chown: chown,
  copyFile: copyFile,
  fchown: fchown,
  futimes: futimes,
  lchown: lchown,
  symlink: symlink,
  utimes: utimes,
  writeFile: writeFile
});

/**
 * @typedef {Object} Future
 * @property {function} bimap
 * @property {function} chain
 * @property {function} fold
 * @property {function} fork
 * @property {function} map
 * @property {function} mapRej
 * @property {function} pipe
 * @property {function} swap
 * It's a future
 */

/**
 * @function readAnyWithFormatOr
 * @param {*} def - default
 * @param {string} format - probably utf8?
 * @param {Array<string>} sources - a list of possible file sources
 * @returns {Future} the first matching file which resolves
 */
const readAnyWithFormatOr = curry((def, format, x) =>
  findF(y => readFile(y, format), def, x)
);

/**
 * @function readAnyWithFormat
 * @param {string} format - probably utf8?
 * @param {Array<string>} sources - a list of possible file sources
 * @returns {Future} the first matching file which resolves
 */
const readAnyWithFormat = readAnyWithFormatOr(null);

/**
 * @function readAny
 * @param {Array<string>} sources - a list of possible file sources
 * @returns {Future} the first matching file which resolves
 */
const readAny = readAnyWithFormat('utf8');

const requireAny = findF(access$1(__, constants.R_OK));

export { access$1 as access, appendFile$1 as appendFile, callbackToTheFutureWithCancel, chmod$1 as chmod, chown$1 as chown, close$1 as close, constants, copyFile$1 as copyFile, createReadStream, createWriteStream, fchmod$1 as fchmod, fchown$1 as fchown, fdatasync$1 as fdatasync, fstat$1 as fstat, fsync$1 as fsync, ftruncate$1 as ftruncate, futimes$1 as futimes, lchmod$1 as lchmod, lchown$1 as lchown, link$1 as link, lstat, mkdir$1 as mkdir, mkdtemp, open, fsParallel as parallel, read, readAny, readFile, readdir, readlink, realpath, rename$1 as rename, requireAny, rmdir$1 as rmdir, stat, symlink$1 as symlink, truncate$1 as truncate, unlink$1 as unlink, utimes$1 as utimes, write, writeFile$1 as writeFile };
